Examples
========

Some examples on how to use WulffPack are collected here. The scripts used in
this tutorial can be downloaded as a `single zip archive
<https://wulffpack.materialsmodeling.org/examples.zip>`_. These scripts will be
compatible with the latest stable release. If you want to download the scripst
from the development version `download this archive
<https://wulffpack.materialsmodeling.org/dev/examples.zip>`_ instead.

.. toctree::
   :maxdepth: 1

   basic_usage
   compare_shapes
   graphics
