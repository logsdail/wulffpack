Core components
===============

.. module:: wulffpack.core

.. index::
   single: Class reference; base_particle

Base particle
-------------

.. autoclass:: BaseParticle
    :members:
    :undoc-members:
    :inherited-members:

.. index::
   single: Class reference; form

Form
----

.. autoclass:: Form
    :members:
    :undoc-members:
    :inherited-members:

.. index::
   single: Class reference; facet

Facet
-----

.. autoclass:: Facet
    :members:
    :undoc-members:
    :inherited-members: